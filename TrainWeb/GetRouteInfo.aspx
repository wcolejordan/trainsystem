﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GetRouteInfo.aspx.cs" Inherits="GetRouteInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Plan Train Routes</title>
</head>
<body>
    <h2>Plan your journey here!</h2>
    <p>Simply select your start point and destination and select whether you would like to know the distance of the journey or fastest route with the next available train.</p>
    <form id="form1" runat="server">
            <table>
                <tr>
                    <td>
                        <p>Start Point:</p>
                    </td>
                    <td>
                        <asp:DropDownList ID="StartDropDown" runat="server" />
                    </td>
                     <td>
                        <p>End Point:</p>
                    </td>
                     <td>
                         <asp:DropDownList ID="EndDropDown" runat="server" />
                    </td>
                    <td>
                        <p>&nbsp;</p>
                    </td>
                    <td>
                        <asp:Button ID="ShortestDistanceButton" runat="server" Text="Get Shortest Distance" Width="191px" OnClick="ShortestDistanceButton_Click" />
                    </td>
                    <td>
                        <asp:Button ID="FastestRouteButton" runat="server" Text="Get Fastest Route" Width="164px" OnClick="FastestRouteButton_Click" />
                    </td>
                </tr>
            </table>
        <asp:TextBox ID="ResultsArea" TextMode="MultiLine" Columns="50" Rows ="25" Enabled="false" runat="server" />
    </form>
    
</body>
</html>
