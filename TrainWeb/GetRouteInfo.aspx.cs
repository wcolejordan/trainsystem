﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrainLibrary;



public partial class GetRouteInfo : System.Web.UI.Page
{
    
    private Database db = Database.GetInstance();
    private localhost.TrainSOAP calcService = new localhost.TrainSOAP();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            updateStations();
        }        
    }

    private void updateStations()
    {
        //Setup dropdowns
        StartDropDown.DataSource = null;
        EndDropDown.DataSource = null;
        StartDropDown.Items.Clear();
        EndDropDown.Items.Clear();
        Dictionary<string, string> stationDict = new Dictionary<string, string>();
        string selectStations = "SELECT DISTINCT train_network.station_id, train_stations.station_name FROM train_network LEFT JOIN train_stations on train_network.station_id = train_stations.station_id";
        var stations = db.select(selectStations);
        bool stationCount = stations.Count > 0;
        if (stationCount)
        {
            foreach (string[] s in stations)
            {
                stationDict.Add(s[0], s[1]);
            }
        }
        else
        {
            stationDict.Add("-1", "None Found");
        }
        StartDropDown.DataSource = stationDict;
        EndDropDown.DataSource = stationDict;
        StartDropDown.DataTextField = "Value";
        EndDropDown.DataTextField = "Value";
        StartDropDown.DataValueField = "Key";
        EndDropDown.DataValueField = "Key";
        StartDropDown.DataBind();
        EndDropDown.DataBind();
    }

    protected void ShortestDistanceButton_Click(object sender, EventArgs e)
    {
        //Get distance from component
        if (StartDropDown.SelectedValue.ToString() != EndDropDown.SelectedValue.ToString())
        {
            string output = calcService.GetRoute(StartDropDown.SelectedValue.ToString(), EndDropDown.SelectedValue.ToString(), 0.001);
            ResultsArea.Text = output;
        }
    }

    protected void FastestRouteButton_Click(object sender, EventArgs e)
    {
        //get time from component
        if (StartDropDown.SelectedValue.ToString() != EndDropDown.SelectedValue.ToString())
        {
            string output = calcService.GetRoute(StartDropDown.SelectedValue.ToString(), EndDropDown.SelectedValue.ToString(), 15);
            ResultsArea.Text = output;
        }
    }
}