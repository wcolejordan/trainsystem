﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainManager
{
    public partial class Alert : Form
    {
        public Alert(string type, string value)
        {
            //Type of alert
            InitializeComponent();
            if (type == "station")
            {
                messageLabel.Text = string.Format("Are you sure you wish to delete {0}?", value);
            }
            else if (type == "line")
            {
                messageLabel.Text = string.Format("Are you sure you wish to delete {0}? All associations will also be deleted.", value);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Close();
            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
            DialogResult = DialogResult.Cancel;
        }
    }
}
