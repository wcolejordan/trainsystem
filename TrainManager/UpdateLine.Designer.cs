﻿namespace TrainManager
{
    partial class UpdateLine
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.positionLabel = new System.Windows.Forms.Label();
            this.confirmButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.positionDropDown = new System.Windows.Forms.ComboBox();
            this.stationDropDown = new System.Windows.Forms.ComboBox();
            this.stationLabel = new System.Windows.Forms.Label();
            this.distanceLabel = new System.Windows.Forms.Label();
            this.errorLabel = new System.Windows.Forms.Label();
            this.distanceBox = new TrainLibrary.Distance();
            this.SuspendLayout();
            // 
            // positionLabel
            // 
            this.positionLabel.AutoSize = true;
            this.positionLabel.Location = new System.Drawing.Point(17, 39);
            this.positionLabel.Name = "positionLabel";
            this.positionLabel.Size = new System.Drawing.Size(58, 17);
            this.positionLabel.TabIndex = 0;
            this.positionLabel.Text = "Position";
            // 
            // confirmButton
            // 
            this.confirmButton.Location = new System.Drawing.Point(20, 193);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(151, 23);
            this.confirmButton.TabIndex = 1;
            this.confirmButton.Text = "Confirm";
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.confirmButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(193, 193);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(151, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // positionDropDown
            // 
            this.positionDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.positionDropDown.FormattingEnabled = true;
            this.positionDropDown.Location = new System.Drawing.Point(223, 36);
            this.positionDropDown.Name = "positionDropDown";
            this.positionDropDown.Size = new System.Drawing.Size(121, 24);
            this.positionDropDown.TabIndex = 3;
            // 
            // stationDropDown
            // 
            this.stationDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stationDropDown.FormattingEnabled = true;
            this.stationDropDown.Location = new System.Drawing.Point(223, 79);
            this.stationDropDown.Name = "stationDropDown";
            this.stationDropDown.Size = new System.Drawing.Size(121, 24);
            this.stationDropDown.TabIndex = 4;
            // 
            // stationLabel
            // 
            this.stationLabel.AutoSize = true;
            this.stationLabel.Location = new System.Drawing.Point(17, 82);
            this.stationLabel.Name = "stationLabel";
            this.stationLabel.Size = new System.Drawing.Size(52, 17);
            this.stationLabel.TabIndex = 5;
            this.stationLabel.Text = "Station";
            // 
            // distanceLabel
            // 
            this.distanceLabel.AutoSize = true;
            this.distanceLabel.Location = new System.Drawing.Point(17, 125);
            this.distanceLabel.Name = "distanceLabel";
            this.distanceLabel.Size = new System.Drawing.Size(199, 17);
            this.distanceLabel.TabIndex = 6;
            this.distanceLabel.Text = "Distance from previous station";
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(17, 160);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(0, 17);
            this.errorLabel.TabIndex = 8;
            // 
            // distanceBox
            // 
            this.distanceBox.Location = new System.Drawing.Point(223, 125);
            this.distanceBox.Name = "distanceBox";
            this.distanceBox.Size = new System.Drawing.Size(121, 22);
            this.distanceBox.TabIndex = 9;
            // 
            // UpdateLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 228);
            this.Controls.Add(this.distanceBox);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.distanceLabel);
            this.Controls.Add(this.stationLabel);
            this.Controls.Add(this.stationDropDown);
            this.Controls.Add(this.positionDropDown);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.confirmButton);
            this.Controls.Add(this.positionLabel);
            this.Name = "UpdateLine";
            this.Text = "UpdateLine";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label positionLabel;
        private System.Windows.Forms.Button confirmButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox positionDropDown;
        private System.Windows.Forms.ComboBox stationDropDown;
        private System.Windows.Forms.Label stationLabel;
        private System.Windows.Forms.Label distanceLabel;
        private System.Windows.Forms.Label errorLabel;
        private TrainLibrary.Distance distanceBox;
    }
}