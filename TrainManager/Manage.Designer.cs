﻿namespace TrainManager
{
    partial class Manage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.networkTab = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.deleteRouteButton = new System.Windows.Forms.Button();
            this.editRouteButton = new System.Windows.Forms.Button();
            this.insertButton = new System.Windows.Forms.Button();
            this.routeListView = new System.Windows.Forms.ListView();
            this.routeLabel = new System.Windows.Forms.Label();
            this.selectLineLabel = new System.Windows.Forms.Label();
            this.lineDropDown = new System.Windows.Forms.ComboBox();
            this.lineTab = new System.Windows.Forms.TabPage();
            this.editLine = new System.Windows.Forms.Button();
            this.lineErrorLabel = new System.Windows.Forms.Label();
            this.addLineBox = new System.Windows.Forms.TextBox();
            this.addLineLabel = new System.Windows.Forms.Label();
            this.addLineButton = new System.Windows.Forms.Button();
            this.deleteLineButton = new System.Windows.Forms.Button();
            this.existingLineLabel = new System.Windows.Forms.Label();
            this.lineListBox = new System.Windows.Forms.ListBox();
            this.stationTab = new System.Windows.Forms.TabPage();
            this.editStationButton = new System.Windows.Forms.Button();
            this.stationErrorLabel = new System.Windows.Forms.Label();
            this.addStationBox = new System.Windows.Forms.TextBox();
            this.addStationLabel = new System.Windows.Forms.Label();
            this.addStationButton = new System.Windows.Forms.Button();
            this.deleteStationButton = new System.Windows.Forms.Button();
            this.existingStationLabel = new System.Windows.Forms.Label();
            this.stationListBox = new System.Windows.Forms.ListBox();
            this.dashboardTab = new System.Windows.Forms.TabPage();
            this.visualiseBox = new System.Windows.Forms.PictureBox();
            this.calcErrorLabel = new System.Windows.Forms.Label();
            this.toDropDown = new System.Windows.Forms.ComboBox();
            this.fromDropDown = new System.Windows.Forms.ComboBox();
            this.outputArea = new System.Windows.Forms.TextBox();
            this.lowestTimeButton = new System.Windows.Forms.Button();
            this.toLabel = new System.Windows.Forms.Label();
            this.fromLabel = new System.Windows.Forms.Label();
            this.getDistances = new System.Windows.Forms.Button();
            this.tabController = new System.Windows.Forms.TabControl();
            this.delayTab = new System.Windows.Forms.TabPage();
            this.delayTextBox = new TrainLibrary.Distance();
            this.delayLabel = new System.Windows.Forms.Label();
            this.removeAllDelays = new System.Windows.Forms.Button();
            this.removeDelayButton = new System.Windows.Forms.Button();
            this.addDelayButton = new System.Windows.Forms.Button();
            this.lineDelayLabel = new System.Windows.Forms.Label();
            this.lineDelayDropDown = new System.Windows.Forms.ComboBox();
            this.distances = new TrainLibrary.DistanceCalculation(this.components);
            this.visualiseNetwork = new TrainLibrary.VisualiseNetwork(this.components);
            this.networkTab.SuspendLayout();
            this.lineTab.SuspendLayout();
            this.stationTab.SuspendLayout();
            this.dashboardTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visualiseBox)).BeginInit();
            this.tabController.SuspendLayout();
            this.delayTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // networkTab
            // 
            this.networkTab.Controls.Add(this.label1);
            this.networkTab.Controls.Add(this.deleteRouteButton);
            this.networkTab.Controls.Add(this.editRouteButton);
            this.networkTab.Controls.Add(this.insertButton);
            this.networkTab.Controls.Add(this.routeListView);
            this.networkTab.Controls.Add(this.routeLabel);
            this.networkTab.Controls.Add(this.selectLineLabel);
            this.networkTab.Controls.Add(this.lineDropDown);
            this.networkTab.Location = new System.Drawing.Point(4, 25);
            this.networkTab.Name = "networkTab";
            this.networkTab.Padding = new System.Windows.Forms.Padding(3);
            this.networkTab.Size = new System.Drawing.Size(1547, 749);
            this.networkTab.TabIndex = 1;
            this.networkTab.Text = "Manage Network";
            this.networkTab.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 409);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(396, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "It is not possible to delete the first station, please edit instead.";
            // 
            // deleteRouteButton
            // 
            this.deleteRouteButton.Location = new System.Drawing.Point(244, 377);
            this.deleteRouteButton.Name = "deleteRouteButton";
            this.deleteRouteButton.Size = new System.Drawing.Size(119, 23);
            this.deleteRouteButton.TabIndex = 7;
            this.deleteRouteButton.Text = "Delete Selected";
            this.deleteRouteButton.UseVisualStyleBackColor = true;
            this.deleteRouteButton.Click += new System.EventHandler(this.deleteRouteButton_Click);
            // 
            // editRouteButton
            // 
            this.editRouteButton.Location = new System.Drawing.Point(135, 377);
            this.editRouteButton.Name = "editRouteButton";
            this.editRouteButton.Size = new System.Drawing.Size(103, 23);
            this.editRouteButton.TabIndex = 6;
            this.editRouteButton.Text = "Edit Selected";
            this.editRouteButton.UseVisualStyleBackColor = true;
            this.editRouteButton.Click += new System.EventHandler(this.editRouteButton_Click);
            // 
            // insertButton
            // 
            this.insertButton.Location = new System.Drawing.Point(19, 377);
            this.insertButton.Name = "insertButton";
            this.insertButton.Size = new System.Drawing.Size(110, 23);
            this.insertButton.TabIndex = 5;
            this.insertButton.Text = "Insert";
            this.insertButton.UseVisualStyleBackColor = true;
            this.insertButton.Click += new System.EventHandler(this.insertButton_Click);
            // 
            // routeListView
            // 
            this.routeListView.FullRowSelect = true;
            this.routeListView.Location = new System.Drawing.Point(19, 82);
            this.routeListView.Name = "routeListView";
            this.routeListView.Size = new System.Drawing.Size(494, 276);
            this.routeListView.TabIndex = 4;
            this.routeListView.UseCompatibleStateImageBehavior = false;
            // 
            // routeLabel
            // 
            this.routeLabel.AutoSize = true;
            this.routeLabel.Location = new System.Drawing.Point(16, 51);
            this.routeLabel.Name = "routeLabel";
            this.routeLabel.Size = new System.Drawing.Size(97, 17);
            this.routeLabel.TabIndex = 3;
            this.routeLabel.Text = "Current Route";
            // 
            // selectLineLabel
            // 
            this.selectLineLabel.AutoSize = true;
            this.selectLineLabel.Location = new System.Drawing.Point(16, 19);
            this.selectLineLabel.Name = "selectLineLabel";
            this.selectLineLabel.Size = new System.Drawing.Size(156, 17);
            this.selectLineLabel.TabIndex = 1;
            this.selectLineLabel.Text = "Select a line to manage";
            // 
            // lineDropDown
            // 
            this.lineDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lineDropDown.FormattingEnabled = true;
            this.lineDropDown.Location = new System.Drawing.Point(178, 16);
            this.lineDropDown.Name = "lineDropDown";
            this.lineDropDown.Size = new System.Drawing.Size(185, 24);
            this.lineDropDown.TabIndex = 0;
            this.lineDropDown.SelectedIndexChanged += new System.EventHandler(this.lineDropDown_SelectedIndexChanged);
            // 
            // lineTab
            // 
            this.lineTab.Controls.Add(this.editLine);
            this.lineTab.Controls.Add(this.lineErrorLabel);
            this.lineTab.Controls.Add(this.addLineBox);
            this.lineTab.Controls.Add(this.addLineLabel);
            this.lineTab.Controls.Add(this.addLineButton);
            this.lineTab.Controls.Add(this.deleteLineButton);
            this.lineTab.Controls.Add(this.existingLineLabel);
            this.lineTab.Controls.Add(this.lineListBox);
            this.lineTab.Location = new System.Drawing.Point(4, 25);
            this.lineTab.Name = "lineTab";
            this.lineTab.Padding = new System.Windows.Forms.Padding(3);
            this.lineTab.Size = new System.Drawing.Size(1547, 749);
            this.lineTab.TabIndex = 1;
            this.lineTab.Text = "Lines";
            this.lineTab.UseVisualStyleBackColor = true;
            // 
            // editLine
            // 
            this.editLine.Location = new System.Drawing.Point(130, 374);
            this.editLine.Name = "editLine";
            this.editLine.Size = new System.Drawing.Size(122, 23);
            this.editLine.TabIndex = 15;
            this.editLine.Text = "Edit";
            this.editLine.UseVisualStyleBackColor = true;
            this.editLine.Click += new System.EventHandler(this.editLine_Click);
            // 
            // lineErrorLabel
            // 
            this.lineErrorLabel.AutoSize = true;
            this.lineErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.lineErrorLabel.Location = new System.Drawing.Point(7, 84);
            this.lineErrorLabel.Name = "lineErrorLabel";
            this.lineErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.lineErrorLabel.TabIndex = 14;
            // 
            // addLineBox
            // 
            this.addLineBox.Location = new System.Drawing.Point(6, 27);
            this.addLineBox.Name = "addLineBox";
            this.addLineBox.Size = new System.Drawing.Size(246, 22);
            this.addLineBox.TabIndex = 13;
            // 
            // addLineLabel
            // 
            this.addLineLabel.AutoSize = true;
            this.addLineLabel.Location = new System.Drawing.Point(3, 7);
            this.addLineLabel.Name = "addLineLabel";
            this.addLineLabel.Size = new System.Drawing.Size(76, 17);
            this.addLineLabel.TabIndex = 12;
            this.addLineLabel.Text = "Line Name";
            // 
            // addLineButton
            // 
            this.addLineButton.Location = new System.Drawing.Point(6, 55);
            this.addLineButton.Name = "addLineButton";
            this.addLineButton.Size = new System.Drawing.Size(118, 23);
            this.addLineButton.TabIndex = 11;
            this.addLineButton.Text = "Add Line";
            this.addLineButton.UseVisualStyleBackColor = true;
            this.addLineButton.Click += new System.EventHandler(this.addLineButton_Click);
            // 
            // deleteLineButton
            // 
            this.deleteLineButton.Location = new System.Drawing.Point(6, 374);
            this.deleteLineButton.Name = "deleteLineButton";
            this.deleteLineButton.Size = new System.Drawing.Size(118, 23);
            this.deleteLineButton.TabIndex = 10;
            this.deleteLineButton.Text = "Delete";
            this.deleteLineButton.UseVisualStyleBackColor = true;
            this.deleteLineButton.Click += new System.EventHandler(this.deleteLineButton_Click);
            // 
            // existingLineLabel
            // 
            this.existingLineLabel.AutoSize = true;
            this.existingLineLabel.Location = new System.Drawing.Point(3, 104);
            this.existingLineLabel.Name = "existingLineLabel";
            this.existingLineLabel.Size = new System.Drawing.Size(94, 17);
            this.existingLineLabel.TabIndex = 8;
            this.existingLineLabel.Text = "Existing Lines";
            // 
            // lineListBox
            // 
            this.lineListBox.FormattingEnabled = true;
            this.lineListBox.ItemHeight = 16;
            this.lineListBox.Location = new System.Drawing.Point(6, 124);
            this.lineListBox.MultiColumn = true;
            this.lineListBox.Name = "lineListBox";
            this.lineListBox.Size = new System.Drawing.Size(246, 244);
            this.lineListBox.TabIndex = 7;
            // 
            // stationTab
            // 
            this.stationTab.Controls.Add(this.editStationButton);
            this.stationTab.Controls.Add(this.stationErrorLabel);
            this.stationTab.Controls.Add(this.addStationBox);
            this.stationTab.Controls.Add(this.addStationLabel);
            this.stationTab.Controls.Add(this.addStationButton);
            this.stationTab.Controls.Add(this.deleteStationButton);
            this.stationTab.Controls.Add(this.existingStationLabel);
            this.stationTab.Controls.Add(this.stationListBox);
            this.stationTab.Location = new System.Drawing.Point(4, 25);
            this.stationTab.Name = "stationTab";
            this.stationTab.Padding = new System.Windows.Forms.Padding(3);
            this.stationTab.Size = new System.Drawing.Size(1547, 749);
            this.stationTab.TabIndex = 1;
            this.stationTab.Text = "Stations";
            this.stationTab.UseVisualStyleBackColor = true;
            // 
            // editStationButton
            // 
            this.editStationButton.Location = new System.Drawing.Point(130, 374);
            this.editStationButton.Name = "editStationButton";
            this.editStationButton.Size = new System.Drawing.Size(122, 23);
            this.editStationButton.TabIndex = 16;
            this.editStationButton.Text = "Edit";
            this.editStationButton.UseVisualStyleBackColor = true;
            this.editStationButton.Click += new System.EventHandler(this.editStationButton_Click);
            // 
            // stationErrorLabel
            // 
            this.stationErrorLabel.AutoSize = true;
            this.stationErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.stationErrorLabel.Location = new System.Drawing.Point(6, 81);
            this.stationErrorLabel.Name = "stationErrorLabel";
            this.stationErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.stationErrorLabel.TabIndex = 15;
            // 
            // addStationBox
            // 
            this.addStationBox.Location = new System.Drawing.Point(6, 27);
            this.addStationBox.Name = "addStationBox";
            this.addStationBox.Size = new System.Drawing.Size(246, 22);
            this.addStationBox.TabIndex = 6;
            // 
            // addStationLabel
            // 
            this.addStationLabel.AutoSize = true;
            this.addStationLabel.Location = new System.Drawing.Point(3, 7);
            this.addStationLabel.Name = "addStationLabel";
            this.addStationLabel.Size = new System.Drawing.Size(93, 17);
            this.addStationLabel.TabIndex = 5;
            this.addStationLabel.Text = "Station Name";
            // 
            // addStationButton
            // 
            this.addStationButton.Location = new System.Drawing.Point(6, 55);
            this.addStationButton.Name = "addStationButton";
            this.addStationButton.Size = new System.Drawing.Size(118, 23);
            this.addStationButton.TabIndex = 4;
            this.addStationButton.Text = "Add Station";
            this.addStationButton.UseVisualStyleBackColor = true;
            this.addStationButton.Click += new System.EventHandler(this.addStationButton_Click);
            // 
            // deleteStationButton
            // 
            this.deleteStationButton.Location = new System.Drawing.Point(6, 374);
            this.deleteStationButton.Name = "deleteStationButton";
            this.deleteStationButton.Size = new System.Drawing.Size(118, 23);
            this.deleteStationButton.TabIndex = 3;
            this.deleteStationButton.Text = "Delete";
            this.deleteStationButton.UseVisualStyleBackColor = true;
            this.deleteStationButton.Click += new System.EventHandler(this.deleteStationButton_Click);
            // 
            // existingStationLabel
            // 
            this.existingStationLabel.AutoSize = true;
            this.existingStationLabel.Location = new System.Drawing.Point(3, 104);
            this.existingStationLabel.Name = "existingStationLabel";
            this.existingStationLabel.Size = new System.Drawing.Size(111, 17);
            this.existingStationLabel.TabIndex = 1;
            this.existingStationLabel.Text = "Existing Stations";
            // 
            // stationListBox
            // 
            this.stationListBox.FormattingEnabled = true;
            this.stationListBox.ItemHeight = 16;
            this.stationListBox.Location = new System.Drawing.Point(6, 124);
            this.stationListBox.MultiColumn = true;
            this.stationListBox.Name = "stationListBox";
            this.stationListBox.Size = new System.Drawing.Size(246, 244);
            this.stationListBox.TabIndex = 0;
            // 
            // dashboardTab
            // 
            this.dashboardTab.Controls.Add(this.visualiseBox);
            this.dashboardTab.Controls.Add(this.calcErrorLabel);
            this.dashboardTab.Controls.Add(this.toDropDown);
            this.dashboardTab.Controls.Add(this.fromDropDown);
            this.dashboardTab.Controls.Add(this.outputArea);
            this.dashboardTab.Controls.Add(this.lowestTimeButton);
            this.dashboardTab.Controls.Add(this.toLabel);
            this.dashboardTab.Controls.Add(this.fromLabel);
            this.dashboardTab.Controls.Add(this.getDistances);
            this.dashboardTab.Location = new System.Drawing.Point(4, 25);
            this.dashboardTab.Name = "dashboardTab";
            this.dashboardTab.Padding = new System.Windows.Forms.Padding(3);
            this.dashboardTab.Size = new System.Drawing.Size(1547, 749);
            this.dashboardTab.TabIndex = 0;
            this.dashboardTab.Text = "Dashboard";
            this.dashboardTab.UseVisualStyleBackColor = true;
            // 
            // visualiseBox
            // 
            this.visualiseBox.Location = new System.Drawing.Point(502, 26);
            this.visualiseBox.Name = "visualiseBox";
            this.visualiseBox.Size = new System.Drawing.Size(1000, 702);
            this.visualiseBox.TabIndex = 9;
            this.visualiseBox.TabStop = false;
            // 
            // calcErrorLabel
            // 
            this.calcErrorLabel.AutoSize = true;
            this.calcErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.calcErrorLabel.Location = new System.Drawing.Point(61, 94);
            this.calcErrorLabel.Name = "calcErrorLabel";
            this.calcErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.calcErrorLabel.TabIndex = 8;
            // 
            // toDropDown
            // 
            this.toDropDown.FormattingEnabled = true;
            this.toDropDown.Location = new System.Drawing.Point(64, 58);
            this.toDropDown.Name = "toDropDown";
            this.toDropDown.Size = new System.Drawing.Size(179, 24);
            this.toDropDown.TabIndex = 7;
            // 
            // fromDropDown
            // 
            this.fromDropDown.FormattingEnabled = true;
            this.fromDropDown.Location = new System.Drawing.Point(64, 26);
            this.fromDropDown.Name = "fromDropDown";
            this.fromDropDown.Size = new System.Drawing.Size(179, 24);
            this.fromDropDown.TabIndex = 6;
            // 
            // outputArea
            // 
            this.outputArea.Location = new System.Drawing.Point(64, 132);
            this.outputArea.Multiline = true;
            this.outputArea.Name = "outputArea";
            this.outputArea.ReadOnly = true;
            this.outputArea.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputArea.Size = new System.Drawing.Size(324, 564);
            this.outputArea.TabIndex = 5;
            // 
            // lowestTimeButton
            // 
            this.lowestTimeButton.Location = new System.Drawing.Point(249, 58);
            this.lowestTimeButton.Name = "lowestTimeButton";
            this.lowestTimeButton.Size = new System.Drawing.Size(139, 23);
            this.lowestTimeButton.TabIndex = 4;
            this.lowestTimeButton.Text = "Get Fastest Route";
            this.lowestTimeButton.UseVisualStyleBackColor = true;
            this.lowestTimeButton.Click += new System.EventHandler(this.lowestTimeButton_Click);
            // 
            // toLabel
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.Location = new System.Drawing.Point(29, 61);
            this.toLabel.Name = "toLabel";
            this.toLabel.Size = new System.Drawing.Size(29, 17);
            this.toLabel.TabIndex = 3;
            this.toLabel.Text = "To:";
            // 
            // fromLabel
            // 
            this.fromLabel.AutoSize = true;
            this.fromLabel.Location = new System.Drawing.Point(14, 29);
            this.fromLabel.Name = "fromLabel";
            this.fromLabel.Size = new System.Drawing.Size(44, 17);
            this.fromLabel.TabIndex = 2;
            this.fromLabel.Text = "From:";
            // 
            // getDistances
            // 
            this.getDistances.Location = new System.Drawing.Point(249, 27);
            this.getDistances.Name = "getDistances";
            this.getDistances.Size = new System.Drawing.Size(139, 23);
            this.getDistances.TabIndex = 1;
            this.getDistances.Text = "Find shortest path";
            this.getDistances.UseVisualStyleBackColor = true;
            this.getDistances.Click += new System.EventHandler(this.getDistances_Click);
            // 
            // tabController
            // 
            this.tabController.Controls.Add(this.dashboardTab);
            this.tabController.Controls.Add(this.stationTab);
            this.tabController.Controls.Add(this.lineTab);
            this.tabController.Controls.Add(this.networkTab);
            this.tabController.Controls.Add(this.delayTab);
            this.tabController.Location = new System.Drawing.Point(12, 12);
            this.tabController.Name = "tabController";
            this.tabController.SelectedIndex = 0;
            this.tabController.Size = new System.Drawing.Size(1555, 778);
            this.tabController.TabIndex = 0;
            // 
            // delayTab
            // 
            this.delayTab.Controls.Add(this.delayTextBox);
            this.delayTab.Controls.Add(this.delayLabel);
            this.delayTab.Controls.Add(this.removeAllDelays);
            this.delayTab.Controls.Add(this.removeDelayButton);
            this.delayTab.Controls.Add(this.addDelayButton);
            this.delayTab.Controls.Add(this.lineDelayLabel);
            this.delayTab.Controls.Add(this.lineDelayDropDown);
            this.delayTab.Location = new System.Drawing.Point(4, 25);
            this.delayTab.Name = "delayTab";
            this.delayTab.Padding = new System.Windows.Forms.Padding(3);
            this.delayTab.Size = new System.Drawing.Size(1547, 749);
            this.delayTab.TabIndex = 1;
            this.delayTab.Text = "Delays";
            this.delayTab.UseVisualStyleBackColor = true;
            // 
            // delayTextBox
            // 
            this.delayTextBox.Location = new System.Drawing.Point(111, 74);
            this.delayTextBox.Name = "delayTextBox";
            this.delayTextBox.Size = new System.Drawing.Size(183, 22);
            this.delayTextBox.TabIndex = 8;
            // 
            // delayLabel
            // 
            this.delayLabel.AutoSize = true;
            this.delayLabel.Location = new System.Drawing.Point(16, 77);
            this.delayLabel.Name = "delayLabel";
            this.delayLabel.Size = new System.Drawing.Size(91, 17);
            this.delayLabel.TabIndex = 7;
            this.delayLabel.Text = "Delay (mins):";
            // 
            // removeAllDelays
            // 
            this.removeAllDelays.Location = new System.Drawing.Point(40, 224);
            this.removeAllDelays.Name = "removeAllDelays";
            this.removeAllDelays.Size = new System.Drawing.Size(254, 33);
            this.removeAllDelays.TabIndex = 5;
            this.removeAllDelays.Text = "Remove All Delays";
            this.removeAllDelays.UseVisualStyleBackColor = true;
            this.removeAllDelays.Click += new System.EventHandler(this.removeAllDelays_Click);
            // 
            // removeDelayButton
            // 
            this.removeDelayButton.Location = new System.Drawing.Point(178, 165);
            this.removeDelayButton.Name = "removeDelayButton";
            this.removeDelayButton.Size = new System.Drawing.Size(116, 32);
            this.removeDelayButton.TabIndex = 4;
            this.removeDelayButton.Text = "Remove Delay";
            this.removeDelayButton.UseVisualStyleBackColor = true;
            this.removeDelayButton.Click += new System.EventHandler(this.removeDelayButton_Click);
            // 
            // addDelayButton
            // 
            this.addDelayButton.Location = new System.Drawing.Point(40, 165);
            this.addDelayButton.Name = "addDelayButton";
            this.addDelayButton.Size = new System.Drawing.Size(120, 32);
            this.addDelayButton.TabIndex = 3;
            this.addDelayButton.Text = "Add Delay";
            this.addDelayButton.UseVisualStyleBackColor = true;
            this.addDelayButton.Click += new System.EventHandler(this.addDelayButton_Click);
            // 
            // lineDelayLabel
            // 
            this.lineDelayLabel.AutoSize = true;
            this.lineDelayLabel.Location = new System.Drawing.Point(16, 19);
            this.lineDelayLabel.Name = "lineDelayLabel";
            this.lineDelayLabel.Size = new System.Drawing.Size(89, 17);
            this.lineDelayLabel.TabIndex = 2;
            this.lineDelayLabel.Text = "Select a line:";
            // 
            // lineDelayDropDown
            // 
            this.lineDelayDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lineDelayDropDown.FormattingEnabled = true;
            this.lineDelayDropDown.Location = new System.Drawing.Point(111, 16);
            this.lineDelayDropDown.Name = "lineDelayDropDown";
            this.lineDelayDropDown.Size = new System.Drawing.Size(183, 24);
            this.lineDelayDropDown.TabIndex = 1;
            this.lineDelayDropDown.SelectedIndexChanged += new System.EventHandler(this.lineDelayDropDown_SelectedIndexChanged);
            // 
            // Manage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1636, 831);
            this.Controls.Add(this.tabController);
            this.Name = "Manage";
            this.Text = "Manage";
            this.networkTab.ResumeLayout(false);
            this.networkTab.PerformLayout();
            this.lineTab.ResumeLayout(false);
            this.lineTab.PerformLayout();
            this.stationTab.ResumeLayout(false);
            this.stationTab.PerformLayout();
            this.dashboardTab.ResumeLayout(false);
            this.dashboardTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.visualiseBox)).EndInit();
            this.tabController.ResumeLayout(false);
            this.delayTab.ResumeLayout(false);
            this.delayTab.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private TrainLibrary.DistanceCalculation distances;
        private System.Windows.Forms.TabPage delayTab;
        private System.Windows.Forms.TabPage networkTab;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button deleteRouteButton;
        private System.Windows.Forms.Button editRouteButton;
        private System.Windows.Forms.Button insertButton;
        private System.Windows.Forms.ListView routeListView;
        private System.Windows.Forms.Label routeLabel;
        private System.Windows.Forms.Label selectLineLabel;
        private System.Windows.Forms.ComboBox lineDropDown;
        private System.Windows.Forms.TabPage lineTab;
        private System.Windows.Forms.Button editLine;
        private System.Windows.Forms.Label lineErrorLabel;
        private System.Windows.Forms.TextBox addLineBox;
        private System.Windows.Forms.Label addLineLabel;
        private System.Windows.Forms.Button addLineButton;
        private System.Windows.Forms.Button deleteLineButton;
        private System.Windows.Forms.Label existingLineLabel;
        private System.Windows.Forms.ListBox lineListBox;
        private System.Windows.Forms.TabPage stationTab;
        private System.Windows.Forms.Button editStationButton;
        private System.Windows.Forms.Label stationErrorLabel;
        private System.Windows.Forms.TextBox addStationBox;
        private System.Windows.Forms.Label addStationLabel;
        private System.Windows.Forms.Button addStationButton;
        private System.Windows.Forms.Button deleteStationButton;
        private System.Windows.Forms.Label existingStationLabel;
        private System.Windows.Forms.ListBox stationListBox;
        private System.Windows.Forms.TabPage dashboardTab;
        private System.Windows.Forms.Label calcErrorLabel;
        private System.Windows.Forms.ComboBox toDropDown;
        private System.Windows.Forms.ComboBox fromDropDown;
        private System.Windows.Forms.TextBox outputArea;
        private System.Windows.Forms.Button lowestTimeButton;
        private System.Windows.Forms.Label toLabel;
        private System.Windows.Forms.Label fromLabel;
        private System.Windows.Forms.Button getDistances;
        private System.Windows.Forms.TabControl tabController;
        private System.Windows.Forms.Label lineDelayLabel;
        private System.Windows.Forms.ComboBox lineDelayDropDown;
        private TrainLibrary.Distance delayTextBox;
        private System.Windows.Forms.Label delayLabel;
        private System.Windows.Forms.Button removeAllDelays;
        private System.Windows.Forms.Button removeDelayButton;
        private System.Windows.Forms.Button addDelayButton;
        private System.Windows.Forms.PictureBox visualiseBox;
        private TrainLibrary.VisualiseNetwork visualiseNetwork;
    }
}

