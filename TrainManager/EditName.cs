﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainManager
{
    public partial class EditName : Form
    {
        private Value name;

        public EditName(ref Value name)
        {
            InitializeComponent();
            this.name = name;
            editStationBox.Text = name.value;
        }

        private void confirmButton_Click(object sender, EventArgs e)
        {
            errorLabel.Text = "";
            string station = editStationBox.Text;
            //Check only letters
            if (station.All(x => char.IsLetter(x) || char.IsWhiteSpace(x)) && station != "")
            {
                name.value = station;
                Close();
                DialogResult = DialogResult.OK;
            }
            else if (station == "")
            {
                errorLabel.Text = "Please enter a station name.";
            }
            else
            {
                errorLabel.Text = "Station names cannot contain numbers or symbols.";
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
            DialogResult = DialogResult.Cancel;
        }
    }
}
