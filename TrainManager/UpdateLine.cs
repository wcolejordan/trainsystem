﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainManager
{
    public partial class UpdateLine : Form
    {
        private string type;
        private string line_id;
        private Value query;
        private List<string[]> route;
        private string[] values;

        public UpdateLine(string type, string line_id, List<string[]> route, Dictionary<string, string> stationDict, ref Value query, string[] values=null)
        {
            //Format dropdowns to ensure no errors and all stations added apart from stations already utilised
            this.type = type;
            this.line_id = line_id;
            this.query = query;
            this.route = route;
            this.values = values;
            InitializeComponent();
            if (route.Count == 0)
            {
                positionDropDown.Items.Insert(0, "1");
                positionDropDown.SelectedIndex = 0;
                distanceBox.Text = "0";
                distanceBox.Enabled = false;
            }
            else
            {
                for(int i=1; i <= route.Count(); i++)
                {
                    positionDropDown.Items.Add(i+1);
                }
                foreach(string[] row in route)
                {
                    var item = stationDict.First(value => value.Value == row[3]);
                    stationDict.Remove(item.Key);
                }
                positionDropDown.SelectedIndex = positionDropDown.Items.Count - 1;
            }
            stationDropDown.DataSource = new BindingSource(stationDict, null);
            stationDropDown.DisplayMember = "Value";
            stationDropDown.ValueMember = "Key";

            if (type == "edit")
            {
                positionDropDown.SelectedItem = int.Parse(values[0]);
                positionDropDown.Enabled = false;
                distanceBox.Text = values[1];
                stationDropDown.SelectedValue = values[2];
                if(int.Parse(values[0]) == 1)
                {
                    distanceBox.Enabled = false;
                }

            }
        }

        private void confirmButton_Click(object sender, EventArgs e)
        {
            //Depending on type, inserts or edits
            errorLabel.Text = "";
            string distance = distanceBox.Text;
            string station_id = stationDropDown.SelectedValue.ToString();
            if (!distanceBox.Enabled || distanceBox.Text.Length > 0)
            {
                if (type == "insert")
                {
                    int position = int.Parse(positionDropDown.Text);
                    query.value = string.Format("INSERT INTO train_network (line_id, station_id, line_position, distance_prev_stop) VALUES ('{0}','{1}','{2}','{3}');", line_id, station_id, position, distance);
                    int i = 1;
                    foreach (string[] row in route)
                    {

                        i++;
                        if (i > position)
                        {
                            query.value += string.Format("UPDATE train_network SET line_position='{0}' WHERE net_id='{1}';", i, row[0]);
                        }
                    }
                    Close();
                    DialogResult = DialogResult.OK;
                }
                else if (type == "edit")
                {
                    query.value = string.Format("UPDATE train_network SET station_id='{0}', distance_prev_stop='{1}' WHERE net_id='{2}';", station_id, distance, values[3]);
                    System.Diagnostics.Debug.WriteLine(query.value);
                    Close();
                    DialogResult = DialogResult.OK;
                }
            }
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
            DialogResult = DialogResult.Cancel;
        }
    }
}
