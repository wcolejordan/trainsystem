﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainLibrary;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace TrainManager
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            if (usernameBox.Text != "" && passwordBox.Text != "" && regexItem.IsMatch(usernameBox.Text) && regexItem.IsMatch(passwordBox.Text))
            {
                //Check username and password valid
                Database db = Database.GetInstance();
                string query = string.Format("SELECT * FROM users WHERE username = '{0}' AND password='{1}'", usernameBox.Text, passwordBox.Text);
                var results = db.select(query);
                db.Close();
                if (results.Count > 0)
                {
                    Close();
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    errorLabel.Text = "Incorrect Credentials";
                }
            }
            else
            {
                errorLabel.Text = "Please enter a username and password. \nCannot contain symbols.";
            }
        }
    }
}
