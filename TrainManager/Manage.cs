﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrainLibrary;

namespace TrainManager
{
    public partial class Manage : Form
    {
        //Setup variables
        private Database db = Database.GetInstance();
        private List<string[]> route;
        private Dictionary<string, string> stationDict;
        private bool stationCount = false;
        private bool lineCount = false;
        private Value query = new Value();

        public Manage()
        {
            //Populate fields
            InitializeComponent();
            updateStations();
            updateLines();
            setDelayBox();
            routeListView.View = View.Details;
            routeListView.Columns.Add("Position");
            routeListView.Columns.Add("Station Name");
            routeListView.Columns.Add("Distance");
            routeListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            routeListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            routeListView.MultiSelect = false;
        }

        // Update list boxes and drop downs
        #region

        private void updateStations()
        {
            try
            {
                //Reset values and then fill
                stationListBox.DataSource = null;
                fromDropDown.DataSource = null;
                toDropDown.DataSource = null;
                stationListBox.Items.Clear();
                fromDropDown.Items.Clear();
                toDropDown.Items.Clear();
                stationDict = new Dictionary<string, string>();
                var attachedStationDict = new Dictionary<string, string>();
                string selectStations = "SELECT * FROM train_stations";
                var stations = db.select(selectStations);
                stationCount = stations.Count > 0;
                if (stationCount)
                {
                    //Add id and name as key value pair
                    foreach (string[] s in stations)
                    {
                        stationDict.Add(s[0], s[1]);
                    }
                }
                else
                {
                    stationDict.Add("-1", "None Found");
                }
                string selectAttachedStations = "SELECT DISTINCT train_network.station_id, train_stations.station_name FROM train_network LEFT JOIN train_stations on train_network.station_id = train_stations.station_id";
                var attachedStations = db.select(selectAttachedStations);
                var attachedStationCount = attachedStations.Count > 0;
                if (attachedStationCount)
                {
                    //Add id and name as key value pair
                    foreach (string[] s in attachedStations)
                    {
                        attachedStationDict.Add(s[0], s[1]);
                    }
                }
                else
                {
                    attachedStationDict.Add("-1", "None Found");
                }
                stationListBox.DataSource = new BindingSource(stationDict, null);
                fromDropDown.DataSource = new BindingSource(attachedStationDict, null);
                toDropDown.DataSource = new BindingSource(attachedStationDict, null);
                stationListBox.DisplayMember = "Value";
                fromDropDown.DisplayMember = "Value";
                toDropDown.DisplayMember = "Value";
                stationListBox.ValueMember = "Key";
                fromDropDown.ValueMember = "Key";
                toDropDown.ValueMember = "Key";
            }
            catch { }
            
        }

        private void updateLines()
        {
            try
            {
                //Reset values and then fill
                lineListBox.DataSource = null;
                lineDelayDropDown.DataSource = null;
                lineDropDown.DataSource = null;
                lineListBox.Items.Clear();
                lineDropDown.Items.Clear();
                lineDelayDropDown.Items.Clear();
                Dictionary<string, string> lineDict = new Dictionary<string, string>();
                string selectLines = "SELECT * FROM train_lines";
                var lines = db.select(selectLines);
                lineCount = lines.Count > 0;
                if (lineCount)
                {
                    foreach (string[] s in lines)
                    {
                        //Add id and name as key value pair
                        lineDict.Add(s[0], s[1]);
                    }
                }
                else
                {
                    lineDict.Add("-1", "None Found");
                }
                lineListBox.DisplayMember = "Value";
                lineListBox.ValueMember = "Key";
                lineDropDown.DisplayMember = "Value";
                lineDropDown.ValueMember = "Key";
                lineDelayDropDown.DisplayMember = "Value";
                lineDelayDropDown.ValueMember = "Key";
                lineListBox.DataSource = new BindingSource(lineDict, null);
                lineDropDown.DataSource = new BindingSource(lineDict, null);
                lineDelayDropDown.DataSource = new BindingSource(lineDict, null);
            }
            catch { }
        }

        private void setDelayBox()
        {
            //Check dropdowns populated due to changed event firing when refreshing values
            if (lineDelayDropDown.DataSource != null && lineDelayDropDown.Items != null)
            {
                try
                {
                    string line_id = lineDelayDropDown.SelectedValue.ToString();
                    string delay_query = string.Format("SELECT delay FROM train_lines WHERE line_id = '{0}';", line_id);
                    var delays = db.select(delay_query);
                    string delay = "";
                    foreach (string[] row in delays)
                    {
                        delay = row[0];
                    }
                    delayTextBox.Text = delay;
                }
                catch { }
            }
            
        }
        #endregion

        // Update list views
        #region
        private void updateRouteListView()
        {
            //Clear and set network depending on selected line and get updated network diagram
            routeListView.Items.Clear();
            visualiseBox.Image = visualiseNetwork.getNetworkDiagram();
            try
            {
                string line_id = lineDropDown.SelectedValue.ToString();
                string getRoute = string.Format("SELECT net_id, train_network.station_id, line_position, station_name, distance_prev_stop FROM train_network " +
                    "LEFT JOIN train_stations ON train_network.station_id = train_stations.station_id WHERE line_id='{0}' ORDER BY line_position", line_id);
                route = db.select(getRoute);
                if (route.Count > 0)
                {
                    foreach (string[] row in route)
                    {
                        var item = new ListViewItem(new string[] { row[2], row[3], row[4] });
                        item.Tag = row[0];
                        routeListView.Items.Add(item);
                    }
                }
            }
            catch
            {

            }
            
        }
        #endregion

        // Station Events
        #region
        private void addStationButton_Click(object sender, EventArgs e)
        {
            //Add station as long as not a duplicate and all characters are letters
            try
            {
                stationErrorLabel.Text = "";
                string stationName = addStationBox.Text;
                if (stationName.All(x => char.IsLetter(x) || char.IsWhiteSpace(x)) && stationName != "")
                {
                    string checkName = string.Format("SELECT * from train_stations WHERE station_name='{0}'", stationName);
                    var checkResult = db.select(checkName);
                    if (checkResult.Count < 1)
                    {
                        string query = string.Format("INSERT into train_stations (station_name) VALUES ('{0}')", stationName);
                        db.query(query);
                        updateStations();
                    }
                    else
                    {
                        stationErrorLabel.Text = "Station already exists.";
                    }
                }
                else if (stationName == "")
                {
                    stationErrorLabel.Text = "Please enter a station name.";
                }
                else
                {
                    stationErrorLabel.Text = "Station names cannot contain numbers or symbols.";
                }
            }
            catch { }
            
        }

        private void deleteStationButton_Click(object sender, EventArgs e)
        {
            //Delete station when button confirmed
            try
            {
                stationErrorLabel.Text = "";
                if (stationCount)
                {
                    string station = stationListBox.GetItemText(stationListBox.SelectedItem);
                    string station_id = stationListBox.SelectedValue.ToString();
                    Alert alert = new Alert("station", station);
                    if (alert.ShowDialog() == DialogResult.OK)
                    {
                        string checkAssociations = String.Format("SELECT line_id FROM train_network WHERE station_id='{0}'", station_id);
                        var check = db.select(checkAssociations);
                        if (check.Count == 0)
                        {
                            string delete = string.Format("DELETE FROM train_stations WHERE station_id='{0}'", station_id);
                            db.query(delete);
                        }
                        else
                        {
                            stationErrorLabel.Text = "Cannot delete station if it currently has associations. Please remove these prior to deletion.";
                        }
                    }
                    updateStations();
                }
            }
            catch { }
           
        }

        private void editStationButton_Click(object sender, EventArgs e)
        {
            //Edit when confirmed
            try
            {
                Value stationName = new Value();
                if (stationListBox.SelectedItems.Count > 0)
                {
                    stationName.value = stationListBox.GetItemText(stationListBox.SelectedItem);
                    System.Diagnostics.Debug.WriteLine(stationName.value);
                    string stationID = stationListBox.SelectedValue.ToString();
                    EditName editStation = new EditName(ref stationName);
                    if (editStation.ShowDialog() == DialogResult.OK)
                    {
                        string checkName = string.Format("SELECT * from train_stations WHERE station_name='{0}'", stationName.value);
                        var checkResult = db.select(checkName);
                        if (checkResult.Count < 1)
                        {
                            string editStationQuery = string.Format("UPDATE train_stations SET station_name='{0}' WHERE station_id='{1}';", stationName.value, stationID);
                            db.query(editStationQuery);
                        }
                        else
                        {
                            stationErrorLabel.Text = "Station already exists.";
                        }
                    }
                }
                updateStations();
                updateRouteListView();
            }
            catch
            {

            }
        }
        #endregion

        // Line Events
        #region
        private void addLineButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Check valid line and add
                lineErrorLabel.Text = "";
                string lineName = addLineBox.Text;
                if (lineName.All(x => char.IsLetter(x) || char.IsWhiteSpace(x)) && lineName != "")
                {
                    string checkName = string.Format("SELECT * from train_lines WHERE line_name='{0}'", lineName);
                    var checkResult = db.select(checkName);
                    if (checkResult.Count < 1)
                    {
                        string query = string.Format("INSERT into train_lines (line_name) VALUES ('{0}')", lineName);
                        db.query(query);
                        updateLines();
                        updateStations();
                    }
                    else
                    {
                        lineErrorLabel.Text = "Line already exists.";
                    }
                }
                else if (lineName == "")
                {
                    lineErrorLabel.Text = "Please enter a line name.";
                }
                else
                {
                    lineErrorLabel.Text = "Line names cannot contain numbers or symbols.";
                }
            }
            catch { }
            
        }

        private void deleteLineButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Delete line when confirmed
                lineErrorLabel.Text = "";
                if (lineCount)
                {
                    string line = lineListBox.GetItemText(lineListBox.SelectedItem);
                    string line_id = lineListBox.SelectedValue.ToString();
                    Alert alert = new Alert("line", line);
                    if (alert.ShowDialog() == DialogResult.OK)
                    {
                        string delete = string.Format("DELETE FROM train_lines WHERE line_id='{0}'", line_id);
                        db.query(delete);
                    }
                    updateLines();
                    updateStations();
                    updateRouteListView();
                }
            }
            catch { }
           
        }

        private void editLine_Click(object sender, EventArgs e)
        {
            try
            {
                //Check edit name is valid and update
                Value lineName = new Value();
                if (lineListBox.SelectedItems.Count > 0)
                {
                    lineName.value = lineListBox.GetItemText(lineListBox.SelectedItem);
                    string lineID = lineListBox.SelectedValue.ToString();
                    EditName editStation = new EditName(ref lineName);
                    if (editStation.ShowDialog() == DialogResult.OK)
                    {
                        string checkName = string.Format("SELECT * from train_lines WHERE line_name='{0}'", lineName.value);
                        var checkResult = db.select(checkName);
                        if (checkResult.Count < 1)
                        {
                            string editLineQuery = string.Format("UPDATE train_lines SET line_name='{0}' WHERE line_id='{1}';", lineName.value, lineID);
                            db.query(editLineQuery);
                        }
                        else
                        {
                            lineErrorLabel.Text = "Line already exists.";
                        }
                    }
                }
                updateLines();
                updateStations();
                updateRouteListView();
            }
            catch { }
            
        }
        #endregion

        //Manage Route Events
        #region
        private void lineDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            //update list view 
            if(lineDropDown.DataSource != null)
            {
                updateRouteListView();
            }
        }

        private void insertButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Insert to network table
                if (lineCount && stationCount)
                {
                    string line_id = lineDropDown.SelectedValue.ToString();
                    UpdateLine insert = new UpdateLine("insert", line_id, route, stationDict, ref query);
                    if (insert.ShowDialog() == DialogResult.OK)
                    {
                        db.query(query.value);
                    }

                    updateRouteListView();
                    updateStations();
                }
            }
            catch { }
            
           
        }
        
        private void editRouteButton_Click(object sender, EventArgs e)
        {
            try
            {
                //check valid and update
                if (lineCount && stationCount && routeListView.SelectedItems.Count > 0)
                {
                    string line_id = lineDropDown.SelectedValue.ToString();
                    //remove selected item from route list so it may still be selected in edit window
                    string net_id = routeListView.SelectedItems[0].Tag.ToString();
                    var item = route.Single(row => row[0] == net_id);
                    string station_id = item[1];
                    string position = item[2];
                    string distance = item[4];
                    string[] values = { position, distance, station_id, net_id };
                    List<string[]> updatedRoute = route;
                    updatedRoute.Remove(updatedRoute.Single(row => row[1] == station_id));
                    UpdateLine edit = new UpdateLine("edit", line_id, route, stationDict, ref query, values);
                    if (edit.ShowDialog() == DialogResult.OK)
                    {
                        db.query(query.value);
                    }
                    updateRouteListView();
                    updateStations();
                }
            }
            catch { }
            
        }

        private void deleteRouteButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Delete all necessary entries and update order
                if (lineCount && stationCount && routeListView.SelectedItems.Count > 0)
                {
                    int selected_position = int.Parse(routeListView.SelectedItems[0].Text);
                    if (selected_position != 1)
                    {
                        query.value = string.Format("DELETE FROM train_network WHERE net_id='{0}';", routeListView.SelectedItems[0].Tag.ToString());
                        route.RemoveAt(route.FindIndex(row => row[0] == routeListView.SelectedItems[0].Tag.ToString()));
                        int i = 1;
                        foreach (string[] row in route)
                        {
                            if (i >= selected_position)
                            {
                                query.value += string.Format("UPDATE train_network SET line_position='{0}' WHERE net_id='{1}';", i, row[0]);
                            }
                            i++;
                        }
                        db.query(query.value);
                        updateRouteListView();
                        updateStations();
                    }
                }
            }
            catch { }
            
        }
        #endregion

        //Dashboard
        #region 
        private void getDistances_Click(object sender, EventArgs e)
        {
            //Calls DistanceCalculation component
            calcErrorLabel.Text = "";
            string start_id = fromDropDown.SelectedValue.ToString();
            string end_id = toDropDown.SelectedValue.ToString();
            if(start_id != end_id)
            {
                outputArea.Text = "";
                var output = distances.calculate(start_id, end_id);
                if (output != "")
                {
                    outputArea.Text = output;
                }
            }
            else
            {
                calcErrorLabel.Text = "Please select different stations.";

            }
            
        }

        private void lowestTimeButton_Click(object sender, EventArgs e)
        {
            //Calls DistanceCalculation component
            calcErrorLabel.Text = "";
            string start_id = fromDropDown.SelectedValue.ToString();
            string end_id = toDropDown.SelectedValue.ToString();
            if (start_id != end_id)
            {
                outputArea.Text = "";
                var output = distances.calculate(start_id, end_id, 15);
                if (output != "")
                {
                    outputArea.Text = output;
                }
            }
            else
            {
                calcErrorLabel.Text = "Please select different stations.";

            }
        }
        #endregion

        //Delays
        #region
        private void addDelayButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Add delay
                string line_id = lineDelayDropDown.SelectedValue.ToString();
                string delay = delayTextBox.Text;
                string delay_query = string.Format("UPDATE train_lines SET delay='{0}' WHERE line_id = '{1}';", delay, line_id);
                db.query(delay_query);
            }
            catch { }
            
        }

        private void removeDelayButton_Click(object sender, EventArgs e)
        {
            try
            {
                //Remove delay
                string line_id = lineDelayDropDown.SelectedValue.ToString();
                string delay_query = string.Format("UPDATE train_lines SET delay='0' WHERE line_id = '{0}';", line_id);
                db.query(delay_query);
                setDelayBox();
            }
            catch { }
            
        }

        private void removeAllDelays_Click(object sender, EventArgs e)
        {
            try
            {
                //Remove all delays
                string delay_query = "UPDATE train_lines SET delay='0'";
                var delays = db.select(delay_query);
                setDelayBox();
            }
            catch { }
        }

        private void lineDelayDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Update delay box
            setDelayBox();
        }
        #endregion
    }
}
