﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainLibrary
{
    public class Network
    {
        //Dictionary of all nodes in network
        internal IDictionary<string, DijkstraNode> Nodes { get; private set; }

        public Network()
        {
            Nodes = new Dictionary<string, DijkstraNode>();
        }

        //Create node based on name and add to network
        public void AddNode(string name)
        {
            var node = new DijkstraNode(name);
            Nodes.Add(name, node);
        }

        //Add a connection between two nodes
        public void AddConnection(string fromNode, string toNode, double distance, bool twoWay)
        {
            Nodes[fromNode].AddConnection(Nodes[toNode], distance, twoWay);
        }

        //Clear the network
        public void Clear()
        {
            Nodes.Clear();
        }
    }
}
