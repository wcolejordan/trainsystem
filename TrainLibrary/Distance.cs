﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainLibrary
{
    //inherit from textbox
    public partial class Distance : TextBox
    {        
        //Raised every time text is changed, after key events so this handles colour changes
        protected override void OnTextChanged(EventArgs e)
        {
            try
            {
                //if length isnt zero
                if (this.Text.Length != 0)
                {
                    //parse value and set colour accordingly
                    float testVal = float.Parse(this.Text);
                    if (testVal > 50)
                    {
                        this.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        this.ForeColor = System.Drawing.Color.Black;
                    }
                }
            }
            catch { }
            base.OnTextChanged(e);
        }

        //Standard keypress
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            //handle event when key is not valid values (stopping letters/symbols etc)
            if (e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            //This code was my original attempt before I realised text changed events are raised after key press - only realised when reviewing LOL

            //try
            //{
            //    //if length > 0
            //    if (this.Text.Length != 0)
            //    {
            //        testVal = 0;
            //        //Get current caret location
            //        int caretIndex = this.SelectionStart;
            //        //If backspace
            //        if (e.KeyChar == (char)Keys.Back)
            //        {
            //            //Start of textbox - no change
            //            if(caretIndex == 0)
            //            {
            //                testVal = float.Parse(this.Text);
            //            }
            //            //Only one digit to be delete so set testval to 0
            //            else if (this.Text.Length == 1 && caretIndex == 1)
            //            {
            //                testVal = 0;
            //            }
            //            //Check if caret at end of box, take substring of current text and parse
            //            else if(caretIndex == this.Text.Length)
            //            {
            //                testVal = float.Parse(this.Text.Substring(0, this.Text.Length - 1));
            //            }
            //            //text must be > 1 so split from caret index and parse
            //            else
            //            {
            //                string rstring = this.Text.Substring(0, caretIndex - 1);
            //                string lstring = this.Text.Substring(caretIndex, this.Text.Length - caretIndex);
            //                testVal = float.Parse(rstring + lstring);
            //            }
            //        }
            //        //If delete key
            //        else if (e.KeyChar == (char)Keys.Delete)
            //        {
            //            //Find where caret is and parse appropriately
            //            if(this.Text.Length == 1 && caretIndex == 0)
            //            {
            //                testVal = 0;
            //            }
            //            else if(caretIndex == this.Text.Length)
            //            {
            //                testVal = float.Parse(this.Text);
            //            }
            //            else
            //            {
            //                string rstring = this.Text.Substring(0, caretIndex);
            //                string lstring = this.Text.Substring(caretIndex + 1, this.Text.Length - caretIndex - 1);
            //                testVal = float.Parse(rstring + lstring);
            //            }
            //        }
            //        //If digit, sets te
            //        else if (char.IsDigit(e.KeyChar))
            //        { 
            //            float existingVal = float.Parse(this.Text) * 10;
            //            float newVal = float.Parse(e.KeyChar.ToString());
            //            testVal = existingVal + newVal;
            //        }
            //        else
            //        {
            //            testVal = float.Parse(this.Text);
            //        }
                    
            //    }
            //    else
            //    {
            //        this.ForeColor = System.Drawing.Color.Black;
            //    }
            //    base.OnKeyPress(e);
            //}
            //catch
            //{
            //    e.Handled = true;
            //}
            
        }

        //So is this
        //Checks if command key is pressed
        //protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        //{
        //    //If delete key raise normal key event for correct handling
        //    if (keyData == Keys.Delete)
        //    {
        //        OnKeyPress(new KeyPressEventArgs((Char)Keys.Delete));
        //    }
        //    //process as normal
        //    return base.ProcessCmdKey(ref msg, keyData);

        //}
    }
}
