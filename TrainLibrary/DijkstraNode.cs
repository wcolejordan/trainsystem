﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainLibrary
{
    internal class DijkstraNode
    {
        //Define node key information
        IList<NodeConnection> conns;
        internal string Name { get; private set; }
        internal DijkstraNode previous { get; set; }
        internal double StartDistance { get; set; }
        internal IEnumerable<NodeConnection> enum_conns { get { return conns; } }

        //Create a new node by setting its name and a new list of connections to and from the node
        internal DijkstraNode(string name)
        {
            Name = name;
            conns = new List<NodeConnection>();
        }

        //Add a connection
        internal void AddConnection(DijkstraNode targetNode, double distance, bool twoWay)
        {
            //If target node deosnt exist or is itself, throws exceptions
            if (targetNode == null) throw new ArgumentNullException("targetNode");
            if (targetNode == this) throw new ArgumentException("Node can't connect to itself");

            //Ensures no negative distances
            if (distance <= 0) throw new ArgumentException("Distance can't be negative");

            //Finally add node to network from this to target
            conns.Add(new NodeConnection(targetNode, distance));

            //If two way set, adds connection in reverse
            if (twoWay) targetNode.AddConnection(this, distance, false);
        }


    }
}
