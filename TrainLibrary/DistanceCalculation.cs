﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainLibrary
{
    public partial class DistanceCalculation : Component
    {
        //Get DB instance and set defaults
        private Database db = Database.GetInstance();
        private string start_id = "";
        private string end_id = "";
        Network network;

        public DistanceCalculation()
        {
            //Initialize component
            InitializeComponent();
            init();
        }

        public DistanceCalculation(IContainer container)
        {
            //Initialize component
            container.Add(this);
            InitializeComponent();
            init();
            
        }

        private void init()
        {
            //Create network
            network = new Network();
        }

        public string calculate(string start_id, string end_id, double change_weight = 0.001)
        {
            //Initialise
            var calc = new DistanceCalc();
            this.start_id = start_id;
            this.end_id = end_id;
            refresh(change_weight);

            //Get distances and correct order
            var distances = calc.CalculateDistances(network, start_id, end_id);
            distances.Reverse();
            
            //Begin output
            string output = "";
            //Get start station name
            var start_result = db.select(string.Format("SELECT station_name FROM train_stations WHERE station_id = '{0}'", start_id));
            if (start_result.Count() > 0)
            {
                foreach (string[] r in start_result)
                {
                    output += string.Format("Journey beginning at {0}. \r\n\r\n", r[0]);
                }
            }
            //Setup metrics
            double cumulative_dist = 0;
            double line_distance = 0;
            string prev_station_id = "";
            int i = 0;
            int last = distances.Count() - 1;
            DateTime timeNow = DateTime.Now;
            long ten = TimeSpan.TicksPerMinute * 10;
            DateTime tenMins = new DateTime(((timeNow.Ticks + ten - 1) / ten) * ten);

            //check whether distance or time calc
            if (change_weight > 0.001)
            {
                
                output += string.Format("Current time: \r\n {0} \r\n\r\n", timeNow.ToLocalTime());
                output += string.Format("Next departing train: \r\n {0} \r\n\r\n", tenMins.ToLocalTime());
            }
            //Iterate through path
            foreach (string[] d in distances)
            {
                //Split line_id from station_id and get distance value
                string[] ids = new string[2];
                ids = d[0].Split('|');
                string line_id = ids[0];
                string station_id = ids[1];
                double dist_value = double.Parse(d[1]);

                string station_name = "";
                string line_name = "";
                //Detect if change or first/last and set station and line name
                if (i == 0 || i == last || station_id == prev_station_id)
                {
                    var result = db.select(string.Format("SELECT station_name, line_name FROM train_network LEFT JOIN train_stations ON train_stations.station_id=train_network.station_id " +
                    "LEFT JOIN train_lines ON train_lines.line_id=train_network.line_id WHERE train_network.station_id='{0}' AND train_network.line_id='{1}'", station_id, line_id));
                    if (result.Count() > 0)
                    {
                        foreach (string[] r in result)
                        {
                            station_name = r[0];
                            line_name = r[1];
                        }
                    }
                }
                //add current distance to line distance 
                line_distance += dist_value;
                //Start output
                if (i == 0)
                {
                    output += string.Format("Make your way to the {0} platform. \r\n\r\n", line_name);

                }
                //End output
                if (i == last)
                {
                    if (change_weight > 0.001)
                    {
                        output += string.Format("Travel for {0} minutes until {1}. \r\n\r\n", Math.Round(line_distance, 2), station_name);
                    }
                    else
                    {
                        output += string.Format("Travel {0} miles to {1}. \r\n\r\n", line_distance, station_name);
                    }
                }
                //standard change output depending on type
                else if (station_id == prev_station_id)
                {
                    if (change_weight > 0.001)
                    {
                        output += string.Format("Travel for {0} minutes until {1}. \r\n\r\n", Math.Round(line_distance, 2), station_name);
                        output += string.Format("Change to {0} line - {1} minutes. \r\n\r\n", line_name, change_weight);
                        cumulative_dist += change_weight;
                    }
                    else
                    {
                        output += string.Format("Travel {0} miles to {1}. \r\n\r\n", Math.Round(line_distance, 2), station_name);
                        output += string.Format("Change to {0} line. \r\n\r\n", line_name);
                    }
                    //reset line distance variable
                    line_distance = 0;
                }
                //add current distance to cumulative
                cumulative_dist += dist_value;
                prev_station_id = station_id;
                i++;
            }
            //if time calc
            if (change_weight > 0.001)
            {
                double rounded_total = (double)Math.Round(cumulative_dist, 2);
                long total_ticks = TimeSpan.TicksPerMinute * (long)rounded_total;
                DateTime approx_arrival_time = new DateTime(tenMins.Ticks + total_ticks);
                output += string.Format("Total time: {0} minutes. \r\n\r\n", rounded_total);
                output += string.Format("Approximate arrival time: \r\n {0} \r\n\r\n", approx_arrival_time.ToLocalTime());
            }
            //distance calc
            else
            {
                output += string.Format("Total distance: {0} miles.", Math.Round(cumulative_dist, 2));
            }
            //return output
            return output;
        }

        public void refresh(double change_weight = 0.001)
        {
            //clear network if there is one there
            if(network.Nodes.Count() > 0) network.Clear();
            //get relevant info
            var stations = db.select("SELECT DISTINCT station_id FROM train_network");
            var station_lines = db.select("SELECT DISTINCT station_id, line_id FROM train_network");
            var lines = db.select("SELECT DISTINCT train_network.line_id, delay FROM train_network LEFT JOIN train_lines on train_network.line_id = train_lines.line_id");
            List<string[]> connections = new List<string[]>();

            //Add station-line relations
            foreach (string[] row in station_lines)
            {
                network.AddNode(row[1] + "|" + row[0]);
            }
            //start and end are plain station id - no line relations
            network.AddNode(start_id);
            network.AddNode(end_id);

            // To account for stations being connected to multiple lines, each node is stored with its line_id and station_id e.g.
            // 5 | 10 : line_id | station_id
            // a change weight is applied linking stations on multiple lines
            //Add multi-line station connections
            foreach (string[] row in stations)
            {
                string station_id = row[0];
                var multi_line_station = db.select(string.Format("SELECT DISTINCT line_id FROM train_network WHERE station_id = '{0}'", station_id));
                string[] line_ids = new string[multi_line_station.Count];
                int j = 0;
                foreach(string[] result in multi_line_station)
                {
                    line_ids[j] = result[0];
                    j++;
                }
                for(int i = 0; i < line_ids.Count(); i++)
                {
                    //if start station, set weight and one direction appropriately
                    if (station_id == start_id)
                    {
                       network.AddConnection(station_id, line_ids[i] + "|" + station_id, 0.00001, false);
                    }
                    //else if end, do reverse
                    else if (station_id == end_id)
                    {
                        network.AddConnection(line_ids[i] + "|" + station_id, station_id, 0.00001, false);
                    }

                    for (int n = i + 1; n < line_ids.Count(); n++)
                    {
                        //connect surrounding lines with change time delay
                        network.AddConnection(line_ids[i] + "|" + station_id, line_ids[n] + "|" + station_id, change_weight, true);
                    }
                }
            }
            // connect lines together
            foreach (string[] row in lines)
            {
                var line_info = db.select(string.Format("SELECT station_id, line_position, distance_prev_stop FROM train_network WHERE line_id='{0}' ORDER BY line_position", row[0]));
                double delay = double.Parse(row[1]);
                //set average speed of trains MPH
                double average_speed = 15;

                //Handle if delays and time calc
                // This code will create connections between all nodes on a line taking into account the delay across the line
                // e.g. a -> 5 -> b -> 10 -> c with a delay of 10 will be setup as:
                // a -> b = 15
                // a -> c = 25
                // b -> c = 20
                if (delay > 0 && change_weight > 0.001)
                {
                    if (line_info.Count > 1)
                    {
                        for (int i = 0; i < line_info.Count(); i++)
                        {
                            var current = line_info.ElementAt(i);
                            string current_station_id = current[0];
                            double current_distance = 0;
                            for (int n = i + 1; n < line_info.Count(); n++)
                            {
                                var next = line_info.ElementAt(n);
                                string next_station_id = next[0];
                                double distance_next = double.Parse(next[2]);
                                double delay_distance = ((current_distance + distance_next) / average_speed) + delay;
                                string[] info = new string[3];
                                info[0] = row[0] + "|" + current_station_id;
                                info[1] = row[0] + "|" + next_station_id;
                                info[2] = delay_distance.ToString();
                                connections.Add(info);
                                current_distance += distance_next;
                            }
                        }
                    }
                }
                //setup line without delays
                else
                {
                    string prev_station = "";
                    foreach (string[] _row in line_info)
                    {
                        if (double.Parse(_row[1]) != 1)
                        {
                            string total = _row[2];
                            //set if time calc taking speed into account
                            if (change_weight > 0.001)
                            {
                                double distance = double.Parse(_row[2]);
                                double time = (distance / average_speed);
                                total = time.ToString();
                            }
                            string[] info = new string[3];
                            info[0] = prev_station;
                            info[1] = row[0] + "|" + _row[0];
                            info[2] = total;
                            connections.Add(info);
                        }
                        prev_station = row[0] + "|" + _row[0];
                    }
                }
                
            }     
            if(connections.Count > 0)
            {
                //add all connections
                foreach (string[] row in connections)
                {
                    network.AddConnection(row[0], row[1], double.Parse(row[2]), true);
                }
            }
        }
    }
}
