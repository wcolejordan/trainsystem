﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainLibrary
{
    internal class NodeConnection
    {
        //Target and distance to target getter setters
        internal DijkstraNode Target { get; private set; }
        internal double Distance { get; private set; }

        //Initialize connection
        internal NodeConnection(DijkstraNode target, double distance)
        {
            Target = target;
            Distance = distance;
        }
    }
}
