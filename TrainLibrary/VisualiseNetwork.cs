﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TrainLibrary
{
    public partial class VisualiseNetwork : Component
    {
        //get database instance and setup variables as parallel arrays
        private Database db = Database.GetInstance();
        string[] line_name = new string[0];
        string[] station_name = new string[0];
        int[] position = new int[0];
        int[] x_coord = new int[0];
        int[] y_coord = new int[0];

        public VisualiseNetwork()
        {
            InitializeComponent();
        }

        public VisualiseNetwork(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        public Bitmap getNetworkDiagram()
        {
            //initialize values
            init();
            Bitmap diagram = new Bitmap(1000, 500);
            Graphics graphics = Graphics.FromImage(diagram);
            Font drawFont = new Font("Arial", 10);
            //Setup brushes (this is a limitation, more must be added manually for greater numbers of lines
            Brush[] brushes = new Brush[]
            {
                Brushes.Blue,
                Brushes.Red,
                Brushes.Green,
                Brushes.Yellow,
                Brushes.Black,
                Brushes.Orange,
                Brushes.Purple,
                Brushes.Pink,
                Brushes.Gold
            };
            //If there are values
            if (line_name != null)
            {
                string prev_line = "";
                int prev_x_coord = 0;
                int prev_y_coord = 0;
                int legend_position = 475;
                int j = 1;
                int brush_size = 15;
                graphics.FillRectangle(Brushes.DarkGray, 0, 0, 1000, 500);
                Pen pen = new Pen(brushes[0], brush_size);
                //For each value
                for (int i = 0; i<line_name.Count(); i++)
                {
                    //Draw lines first
                    if (line_name[i] == prev_line)
                    {
                        Point point1 = new Point(prev_x_coord + 16, prev_y_coord + 16);
                        Point point2 = new Point(x_coord[i] + 16, y_coord[i] + 16);
                        graphics.DrawLine(pen, point1, point2);
                    } 
                    //Draw legend with new brush
                    else if (i > 0)
                    {
                        brush_size -= 0;
                        pen = new Pen(brushes[j], brush_size);
                        Point point1 = new Point(15, legend_position + 5);
                        Point point2 = new Point(65, legend_position + 5);
                        graphics.DrawLine(pen, point1, point2);
                        PointF textPoint = new PointF();
                        textPoint.X = 75;
                        textPoint.Y = legend_position;
                        graphics.DrawString(line_name[i], drawFont, Brushes.Black, textPoint);
                        j++;
                        legend_position -= 25;
                    }//Draw initial legend
                    else if (i == 0)
                    {
                        Point point1 = new Point(15, legend_position + 5);
                        Point point2 = new Point(65, legend_position + 5);
                        graphics.DrawLine(pen, point1, point2);
                        PointF textPoint = new PointF();
                        textPoint.X = 75;
                        textPoint.Y = legend_position;
                        graphics.DrawString(line_name[i], drawFont, Brushes.Black, textPoint);
                        legend_position -= 25;
                    }
                    prev_line = line_name[i];
                    prev_x_coord = x_coord[i];
                    prev_y_coord = y_coord[i];
                }
                //Draw stations
                for (int i = 0; i < line_name.Count(); i++)
                {
                    graphics.FillEllipse(Brushes.White, x_coord[i], y_coord[i], 32, 32);
                }
                string[] station_names_filled = new string[line_name.Count()];
                //Add text to stations
                for(int i = 0; i < line_name.Count(); i++)
                {
                    if (!station_names_filled.Contains(station_name[i]))
                    {
                        PointF textPoint = new PointF();
                        textPoint.X = x_coord[i];
                        textPoint.Y = y_coord[i] - 20;
                        graphics.DrawString(station_name[i], drawFont, Brushes.Black, textPoint);
                    }
                    station_names_filled[i] = station_name[i];
                }
                
            }
            //Return the bitmap
            return diagram;
        }

        private void init()
        {
            //Get distinct line_id's and names from network
            var lines_query = "SELECT DISTINCT train_network.line_id, train_lines.line_name FROM train_network LEFT JOIN train_lines on train_network.line_id=train_lines.line_id";
            //Count records
            var get_all = "SELECT COUNT(*) FROM train_network";
            var count = db.select(get_all);
            var lines = db.select(lines_query);
            //Setup parallel arrays
            var count_value = int.Parse(count.ElementAt(0)[0]);
            line_name = new string[count_value];
            station_name = new string[count_value];
            position = new int[count_value];
            x_coord = new int[count_value];
            y_coord = new int[count_value];
            int i = 0;
            int k = 50;
            //For each line
            foreach (string[] line in lines)
            {
                //Get station name and position ordered by position
                string current_line_id = line[0];
                string current_line_name = line[1];
                var network_query = string.Format("SELECT train_stations.station_name, line_position FROM train_network " +
                                        "LEFT JOIN train_stations on train_stations.station_id=train_network.station_ID " +
                                        "LEFT JOIN train_lines on train_lines.line_id=train_network.line_id " +
                                        "WHERE train_network.line_id='{0}' ORDER BY line_position;", current_line_id);
                var network = db.select(network_query);
                for (int j = i; j < (network.Count() + i); j++)
                {
                    //Check if station already exists in network and set coordinates appropiatelty
                    if (station_name.Contains(network.ElementAt(j - i)[0]))
                    {
                        string stat = network.ElementAt(j - i)[0];
                        var count_dupes = station_name.Where(e => e == stat);
                        int index = Array.FindIndex(station_name, x => x.Contains(network.ElementAt(j - i)[0]));
                        x_coord[j] = x_coord[index] + (15 * count_dupes.Count());
                        y_coord[j] = y_coord[index] + (15 * count_dupes.Count());
                    }
                    else
                    {
                        x_coord[j] = ((j - i) * 130) + 50;
                        y_coord[j] = k;
                    }
                    
                    line_name[j] = current_line_name;
                    station_name[j] = network.ElementAt(j - i)[0];
                    position[j] = int.Parse(network.ElementAt(j - i)[1]);
                }
                k += 80;
                i += network.Count();
            }
        }
    }
}

