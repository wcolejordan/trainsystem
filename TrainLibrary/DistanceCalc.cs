﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainLibrary
{
    class DistanceCalc
    {
        private string startNode;

        //Return list of strings containing the path in order
        public List<string[]> CalculateDistances(Network network, string startNode, string endNode)
        {
            this.startNode = startNode;

            //Check start and end exist
            if(!network.Nodes.Any(n => n.Key == startNode) || !network.Nodes.Any(n => n.Key == endNode))
            {
                throw new ArgumentException("Nodes must be in network");
            }
            //Initialize
            InitializeNetwork(network);
            //Process
            ProcessNetwork(network);
            //Get path and return
            return GetPath(network, endNode);

        }

        //Sets all nodes distance from start to infinity then sets start node to 0
        private void InitializeNetwork(Network network)
        {
            foreach(DijkstraNode node in network.Nodes.Values)
            {
                node.StartDistance = double.PositiveInfinity;
            }
            network.Nodes[startNode].StartDistance = 0;
        }

        private void ProcessNetwork(Network network)
        {
            //init boolean for while loop
            bool finished = false;
            //Get all nodes in list form
            var queue = network.Nodes.Values.ToList();
            while(!finished)
            {
                //Order list of nodes by start distance and select top of pile as long as its not infinity
                DijkstraNode nextNode = queue.OrderBy(n => n.StartDistance).FirstOrDefault(n => !double.IsPositiveInfinity(n.StartDistance));
                if(nextNode != null)
                {
                    //Process the node and remove from queue
                    ProcessNode(nextNode, queue);
                    queue.Remove(nextNode);
                    
                }
                else
                {
                    finished = true;
                }
            }
        }

        private void ProcessNode(DijkstraNode node, List<DijkstraNode> queue)
        {
            //Get nodes connected to this node
            var conns = node.enum_conns.Where(c => queue.Contains(c.Target));
            foreach(var conn in conns)
            {
                //for each node add current distance from start to target node distance from current node
                double distance = node.StartDistance + conn.Distance;
                //If this distance is less than the existing distance from start to the given node, replace its path with this node and update the distance
                if (distance < conn.Target.StartDistance)
                {
                    conn.Target.previous = node;
                    conn.Target.StartDistance = distance;
                }
            }
        }

        private List<string[]> GetPath(Network network, string endNode)
        {
            List<string[]> path = new List<string[]>();
            //Get endnode
            DijkstraNode target = network.Nodes[endNode];
            //Recursively iterate from the end node back to the start node
            for(DijkstraNode node = target; node != null; node = node.previous)
            {
                //Get previous node from connections and add to output path
                var conn = node.enum_conns.Where(n => n.Target == node.previous).FirstOrDefault();
                if(conn != null)
                {
                    string[] info = new string[2];
                    info[0] = node.Name;
                    info[1] = conn.Distance.ToString();
                    path.Add(info);
                }
            }
            return path;
        }
    }
}
