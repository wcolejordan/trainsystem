﻿using System;
using System.Collections.Generic;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace TrainLibrary
{
    public class Database
    {
        private Database()
        {
        }

        //Singleton design pattern to ensure only one connection open at a time.
        private MySqlConnection connection = null;
        private static Database instance = null;
        public static Database GetInstance()
        {
            //if instance is not set, set instance
            if (instance == null) instance = new Database();
            //return instance
            return instance;
        }

        //Method opens a new connection if there is no existing connection
        private void GetConnection()
        {
            if (connection == null)
            {
                connection = new MySqlConnection("Server=localhost; database=train_db; UID=root; password=;");
                connection.Open();
            }
            else if (connection.State.ToString() == "Closed")
            {
                connection.Open();
            }
        }

        //This method returns the results from a select query as a list of string arrays
        public List<string[]> select(string query)
        {
            //gets connection
            GetConnection();
            var command = new MySqlCommand(query, connection);
            try
            {
                var reader = command.ExecuteReader();
                int columns = reader.FieldCount;
                List<string[]> results = new List<string[]>();
                //While results exist adds to array then to list
                while (reader.Read())
                {
                    string[] row = new string[columns];
                    for (int i = 0; i < columns; i++)
                    {
                        row[i] = reader.GetString(i);
                    }
                    results.Add(row);
                }
                //Close reader
                reader.Close();
                return results;
            }
            catch (MySqlException e)
            {
                //Outputs exception
                System.Diagnostics.Debug.WriteLine(e);
                return null;
            }
            
            
        }

        //This method executes querys such as insert, update, delete
        public bool query(string query)
        {
            GetConnection();
            var command = new MySqlCommand(query, connection);
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch(MySqlException e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return false;
            }
            
            
        }

        //Closes connections
        public void Close()
        {
            connection.Close();
        }
    }
}
