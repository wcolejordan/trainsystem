﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using TrainLibrary;

/// <summary>
/// Summary description for TrainSOAP
/// </summary>
[WebService(Namespace = "http://localhost/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

public class TrainSOAP : System.Web.Services.WebService
{
    DistanceCalculation distances = new DistanceCalculation();
    public TrainSOAP()
    {
    }

    [WebMethod]
    public string GetRoute(string start_id, string end_id, double change_weight)
    {
        //Calls distance component and returns output
        string output = distances.calculate(start_id, end_id, change_weight);
        return output;
    }

}
