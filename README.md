This project contains four sub projects, each with a different purpose. 

TrainLibrary:  
Provides a number of components, and a class to connect to the database. The components it provides will draw the network and find shortest paths using Dijkstra's algorithm.

TrainManager:  
Windows Forms application that allows an admin to create lines and stations and add relationships between them.

TrainServices:  
SOAP service to query shortest paths between stations.

TrainWeb:  
Simple web application making employs the use of the TrainServices service.